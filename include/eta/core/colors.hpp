#pragma once

#include <ostream>

namespace eta {

enum class color_t {
    blue,
    cyan,
    gray,
    green,
    magenta,
    red,
    standard,
    yellow,
};

enum class style_t {
    bold,
    italic,
    reversed,
    standard,
    underline,
};

auto set_style(std::ostream & os, style_t style) -> void;
auto set_color(std::ostream & os, color_t color) -> void;

auto enable_colors(bool = true) -> void;
auto disable_colors(bool = true) -> void;

}  // namespace eta
