#include "help.hpp"

#include <iostream>

#include "colors.hpp"

namespace eta {

auto print_option(char abbrev, char const * option, char const * descr) -> void {
    set_color(std::cerr, color_t::green);
    std::cerr << "    ";
    if (abbrev != 0) {
        std::cerr << '-' << abbrev;
        set_color(std::cerr, color_t::standard);
        std::cerr << ", ";
    } else
        std::cerr << "    ";
    set_color(std::cerr, color_t::green);
    std::cerr << "--" << option << '\n';
    set_color(std::cerr, color_t::standard);
    std::cerr << "            " << descr << '\n';
}

auto print_section(char const * section) -> void {
    set_color(std::cerr, color_t::yellow);
    std::cerr << section << '\n';
    set_color(std::cerr, color_t::standard);
}

auto print_subsection(char const * subsection) -> void {
    set_color(std::cerr, color_t::standard);
    std::cerr << "  " << subsection << '\n';
}

auto print_error(int err_code, char const * name, char const * descr) -> void {
    set_color(std::cerr, color_t::green);
    std::cerr << "    " << err_code;
    set_color(std::cerr, color_t::standard);
    std::cerr << ", ";
    set_color(std::cerr, color_t::green);
    std::cerr << name;
    set_color(std::cerr, color_t::standard);
    std::cerr << "\n            " << descr << '\n';
}

}  // namespace eta
