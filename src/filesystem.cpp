#include "filesystem.hpp"

#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <cstdlib>
#include <iostream>
#include <string>

#include "debug.hpp"

namespace eta {

constexpr auto invalidPath = "";

// Path implementation ////////////////////////////////////////////////////

struct Path::Impl final {
    std::string currentPath;
    Impl(const char * str) : currentPath { str } {}
};

//-------------------------------------------------------------------------

Path::Path() : _impl(std::make_unique<Impl>(invalidPath)) {}

//-------------------------------------------------------------------------

Path::~Path() = default;

//-------------------------------------------------------------------------

Path::Path(Path && rhs) { std::swap(_impl, rhs._impl); }

//-------------------------------------------------------------------------

Path::Path(const Path & rhs) : _impl(std::make_unique<Impl>(*rhs._impl)) {}

//-------------------------------------------------------------------------

auto Path::operator=(Path && rhs) -> Path & {
    std::swap(_impl, rhs._impl);
    return *this;
}

//-------------------------------------------------------------------------

auto Path::operator=(const Path & rhs) -> Path & {
    _impl->currentPath = rhs._impl->currentPath;
    return *this;
}

//-------------------------------------------------------------------------

auto Path::operator=(const char * rhs) -> Path & {
    _impl = std::make_unique<Impl>(rhs);
    _simplify();
    return *this;
}

//-------------------------------------------------------------------------

Path::Path(const char * path, size_t size) : Path() {
    if (size > 0)
        _impl->currentPath = std::string_view { path, size };
    else
        _impl->currentPath = path;
    _simplify();
}

//-------------------------------------------------------------------------

Path::Path(const std::string_view & str) : Path(str.data(), str.size()) {}

//-------------------------------------------------------------------------

auto Path::operator=(const std::string_view & str) -> Path & {
    _impl->currentPath = str;
    _simplify();
    return *this;
}

//-------------------------------------------------------------------------

auto Path::c_str() const -> const char * { return _impl->currentPath.c_str(); }

//-------------------------------------------------------------------------

auto Path::memory() const -> size_t { return _impl->currentPath.capacity() + sizeof(Impl); }

//-------------------------------------------------------------------------

auto Path::operator/(const Path & rhs) const -> Path { return operator/(rhs.c_str()); }

//-------------------------------------------------------------------------

auto Path::operator/(const char * rhs) const -> Path {
    auto res = *this;
    if (!res._impl->currentPath.empty())
        res._impl->currentPath += "/";
    res._impl->currentPath += rhs;
    res._simplify();
    return res;
}

//-------------------------------------------------------------------------

auto Path::invalid() -> Path { return Path {}; }

//-------------------------------------------------------------------------

auto Path::isValid() const -> bool { return _impl->currentPath != invalidPath; }

//-------------------------------------------------------------------------

auto Path::isInvalid() const -> bool { return _impl->currentPath == invalidPath; }

//-------------------------------------------------------------------------

auto Path::parent() const -> Path {
    Assert(isValid(), "");
    return *this / "..";
}

//-------------------------------------------------------------------------

auto Path::basename() const -> Path {
    Assert(isValid(), "");
    return Path { _basename() };
}

auto Path::dirname() const -> Path {
    Assert(isValid(), "");
    const auto res = _impl->currentPath.find_last_of('/');
    return Path { res == _impl->currentPath.npos ? "./" : _impl->currentPath.substr(0, res) };
}

//-------------------------------------------------------------------------

auto Path::extension() const -> std::string_view {
    auto const str = std::string_view(_impl->currentPath);
    Assert(isValid(), '"', str, '"');

    auto const last_dot = str.find_last_of('.');
    if (last_dot == str.npos)
        return std::string_view("");

    auto const last_slash = _impl->currentPath.find_last_of('/');
    if (last_slash != str.npos && last_slash > last_dot)
        return std::string_view("");

    auto const first_char = last_dot + 1;
    return std::string_view(str.data() + first_char, str.size() - first_char);
}

//-------------------------------------------------------------------------

auto Path::_basename() const -> std::string_view {
    Assert(isValid(), "");
    const auto res = _impl->currentPath.find_last_of('/');
    return res == _impl->currentPath.npos ? c_str() : c_str() + res + 1;
}

//-------------------------------------------------------------------------

auto Path::_simplify() -> void {
    const auto isAbsolute = _impl->currentPath.front() == '/';

    auto tokens = [](std::string_view str) {
        const auto end = str.end();
        auto it = str.begin();
        auto res = std::vector<std::string_view> {};

        auto firstCharOfToken = end;

        while (it != end) {
            if (*it == '/') {
                if (firstCharOfToken != end)
                    res.emplace_back(&firstCharOfToken[0], it - firstCharOfToken);

                firstCharOfToken = end;
            } else if (firstCharOfToken == end) {
                firstCharOfToken = it;
            }
            ++it;
        }

        if (firstCharOfToken != end)
            res.emplace_back(&firstCharOfToken[0], it - firstCharOfToken);

        return res;
    }(_impl->currentPath);

    auto directories = decltype(tokens) {};

    for (auto token : tokens) {
        if (token == "..") {
            if (directories.empty()) {
                if (!isAbsolute)
                    directories.emplace_back("..");
            } else {
                if (directories.back() != "..")
                    directories.pop_back();
                else
                    directories.emplace_back("..");
            }
        } else if (token != ".")
            directories.emplace_back(token);
    }

    auto res = std::string {};

    for (const auto & directory : directories) {
        if (!res.empty() || isAbsolute)
            res += '/';
        res += directory;
    }

    if (res.empty())
        res = isAbsolute ? "/" : ".";

    _impl->currentPath = res;
}

// Free functions /////////////////////////////////////////////////////////

auto currentDirectory() -> Path {
    char buf[PATH_MAX];
    if (getcwd(buf, sizeof(buf)) != nullptr)
        return Path { buf };
    return Path {};
}

//-------------------------------------------------------------------------

auto setCurrentDirectory(const Path & path) -> void {
    Assert(path.isValid(), "");
    if (chdir(path.c_str()) != 0)
        std::cerr << "Failed to set current path to \"" << path.c_str()
                  << "\"\n";  // TODO check errno
}

//-------------------------------------------------------------------------

auto searchParent(const Path & path, const char * _name) -> Path {
    Assert(path.isValid(), "");
    const auto name = Path { _name };
    auto res = path;
    while (true) {
        const auto basename = res.basename();
        if (basename == name)
            return res;
        if (res == Path { "/" })
            return Path::invalid();
        res = res.parent();
    }
}

//-------------------------------------------------------------------------

auto isFile(const Path & path) -> bool {
    Assert(path.isValid(), "");
    struct stat sb;
    return stat(path.c_str(), &sb) == 0 && S_ISREG(sb.st_mode);
}

//-------------------------------------------------------------------------

auto isDirectory(const Path & path) -> bool {
    Assert(path.isValid(), "");
    struct stat sb;
    return stat(path.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode);
}

//-------------------------------------------------------------------------

auto listEntries(const Path & path) -> std::vector<Path> {
    Assert(path.isValid(), '"', path.c_str(), '"');
    Assert(isDirectory(path), '"', path.c_str(), '"');

    const auto dirp = opendir(path.c_str());
    Assert(dirp != nullptr, "");  // TODO check errno

    auto res = std::vector<Path> {};

    while (true) {
        const auto dp = readdir(dirp);
        if (dp == nullptr)
            break;
        res.emplace_back(dp->d_name);
    }

    closedir(dirp);
    return res;
}

//-------------------------------------------------------------------------

}  // namespace eta
