#include "debug.hpp"

namespace eta {

#ifdef NDEBUG
log_level_t log_level = log_level_t::error;
#else
log_level_t log_level = log_level_t::warning;
#endif /* NDEBUG */

}  // namespace eta
