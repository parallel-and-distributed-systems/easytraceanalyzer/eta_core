#pragma once

namespace eta {

auto print_option(char abbrev, char const * option, char const * descr) -> void;
auto print_section(char const * section) -> void;
auto print_subsection(char const * subsection) -> void;
auto print_error(int err_code, char const * name, char const * descr) -> void;

}  // namespace eta
