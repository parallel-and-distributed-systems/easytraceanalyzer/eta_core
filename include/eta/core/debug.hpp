#pragma once

#include <iostream>
#include <string_view>

#define eta_fwd(arg) ::std::forward<decltype(arg)>(arg)

namespace eta {

#define DO_NOTHING ((void)0)

template <typename... T> auto do_nothing(T &&... args) { (((void)std::forward<T>(args), ...)); }

#define STRINGIFY_IMPL(arg) #arg
#define STRINGIFY(arg) STRINGIFY_IMPL(arg)

// PRINT AND WARN ////////////////////////////////

namespace printing {
    template <typename OS> auto print_stream(OS &&) -> void {}
    template <typename OS, typename... Ts> auto print_stream(OS && os, Ts &&... args) -> void {
        ((eta_fwd(os) << eta_fwd(args)), ...);
    }

    template <typename... Ts> auto print(Ts &&... args) -> void {
        print_stream(std::cout, eta_fwd(args)...);
    }

    template <typename... Ts> auto warn(Ts &&... args) -> void {
        print_stream(std::cerr, eta_fwd(args)...);
    }

    template <typename... Ts> auto println(Ts &&... args) -> void {
        print_stream(std::cout, eta_fwd(args)...);
        std::cout << std::endl;
    }
    template <typename... Ts> auto warnln(Ts &&... args) -> void {
        print_stream(std::cerr, eta_fwd(args)...);
        std::cerr << std::endl;
    }
}  // namespace printing

using namespace printing;

// Debug only shortcut

// ASSERTS ///////////////////////////////////////

enum class log_level_t { critical, error, info, warning, debug, trace };
extern log_level_t log_level;

template <auto size, typename... Ts>
auto log_impl(log_level_t lvl, char const (&txt)[size], Ts &&... args) -> void {
    if (lvl > log_level)
        return;
    warnln(txt, ' ', eta_fwd(args)...);
}

template <typename... T> auto log_critical(T &&... args) {
    log_impl(log_level_t::critical, "CRITICAL", eta_fwd(args)...);
}
template <typename... T> auto log_error(T &&... args) {
    log_impl(log_level_t::error, "ERROR   ", eta_fwd(args)...);
}
template <typename... T> auto log_warning(T &&... args) {
    log_impl(log_level_t::warning, "WARNING ", eta_fwd(args)...);
}
template <typename... T> auto log_debug(T &&... args) {
    log_impl(log_level_t::debug, "DEBUG   ", eta_fwd(args)...);
}
template <typename... T> auto log_info(T &&... args) {
    log_impl(log_level_t::info, "INFO    ", eta_fwd(args)...);
}
template <typename... T> auto log_trace(T &&... args) {
    log_impl(log_level_t::trace, "TRACE   ", eta_fwd(args)...);
}

[[maybe_unused]] static auto set_log_level(log_level_t lvl) { log_level = lvl; }

#define AssertImplementation(cond, must_abort, log_fn, ...)                           \
    do {                                                                              \
        if (cond) {                                                                   \
        } else {                                                                      \
            log_impl(::eta::log_level_t::error,                                       \
                     "Assert " __FILE__ "(" STRINGIFY(__LINE__) ") : \"" #cond "\" ", \
                     __VA_ARGS__);                                                    \
            if constexpr (must_abort)                                                 \
                ::abort();                                                            \
        }                                                                             \
    } while (false)

#define CriticalAssert(cond, ...) AssertImplementation(cond, true, critical, __VA_ARGS__)
#ifdef NDEBUG
#    define Assert(cond, ...) DO_NOTHING
#    define AssertWarning(cond, ...) DO_NOTHING
#else
#    define Assert(cond, ...) AssertImplementation(cond, true, error, __VA_ARGS__)
#    define AssertWarning(cond, ...) AssertImplementation(cond, false, warning, __VA_ARGS__)
#endif

}  // namespace eta
