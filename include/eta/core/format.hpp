#pragma once

#include <sstream>
#include <string>

namespace eta {

template <typename... Ts> auto format(Ts &&... args) -> std::string {
    auto stream = std::stringstream {};
    ((stream << args), ...);
    return std::move(stream).str();
}

}  // namespace eta
