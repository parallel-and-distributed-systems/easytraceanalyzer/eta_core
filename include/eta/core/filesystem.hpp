#pragma once

#include <memory>
#include <ostream>
#include <string_view>
#include <vector>

namespace eta {

class Path final {
    struct Impl;

  public:
    Path();
    ~Path();
    Path(Path &&);
    Path(const Path &);
    auto operator=(Path &&) -> Path &;
    auto operator=(const Path &) -> Path &;
    explicit Path(const std::string_view &);
    auto operator=(const char *) -> Path &;
    Path(const char * str, size_t size = 0);
    auto operator=(const std::string_view &) -> Path &;

  public:
    [[nodiscard]] auto c_str() const -> const char *;
    operator const char *() const { return c_str(); }
    [[nodiscard]] auto str() const -> std::string_view { return c_str(); }
    operator std::string_view() const { return str(); }

  public:
    [[nodiscard]] auto memory() const -> std::size_t;

  public:
    [[nodiscard]] auto operator/(const Path &) const -> Path;
    [[nodiscard]] auto operator/(const char *) const -> Path;

  public:
    [[nodiscard]] static auto invalid() -> Path;
    [[nodiscard]] auto isValid() const -> bool;
    [[nodiscard]] auto isInvalid() const -> bool;

  public:
    [[nodiscard]] auto parent() const -> Path;
    [[nodiscard]] auto dirname() const -> Path;
    [[nodiscard]] auto basename() const -> Path;
    [[nodiscard]] auto extension() const -> std::string_view;

  private:
    [[nodiscard]] auto _basename() const -> std::string_view;
    auto _simplify() -> void;

  private:
    std::unique_ptr<Impl> _impl;
};

[[nodiscard]] auto currentDirectory() -> Path;
auto setCurrentDirectory(const Path & path) -> void;
[[nodiscard]] auto searchParent(const Path & path, const char * name) -> Path;
[[nodiscard]] auto isFile(const Path & path) -> bool;
[[nodiscard]] auto isDirectory(const Path & path) -> bool;
[[nodiscard]] auto listEntries(const Path & path) -> std::vector<Path>;

inline auto operator<<(std::ostream & os, Path const & path) -> std::ostream & {
    return os << path.c_str();
}

inline auto operator<(Path const & l, Path const & r) { return l.str() < r.str(); }
inline auto operator>(Path const & l, Path const & r) { return l.str() < r.str(); }
inline auto operator<=(Path const & l, Path const & r) { return l.str() < r.str(); }
inline auto operator>=(Path const & l, Path const & r) { return l.str() < r.str(); }
inline auto operator==(Path const & l, Path const & r) { return l.str() < r.str(); }
inline auto operator!=(Path const & l, Path const & r) { return l.str() < r.str(); }

}  // namespace eta
